package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"user_interest/ui_go_api_gateway/api/docs"
	"user_interest/ui_go_api_gateway/api/handlers"

	"user_interest/ui_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization
	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))
	// r.Use(h.AuthMiddleware())

	// Auth Api
	{
		r.POST("/login", h.Login)
		r.POST("/register", h.Register)
	}

	{
		//checkout service

		r.POST("/open-shift", h.OpenShift)
		r.POST("/close-shift/:id", h.CloseShift)
		r.POST("/barcode/:sale_id/:barcode/:quantity", h.ScanBarcode)
		r.PUT("/do_sale/:id", h.DoSale)

		r.POST("/sale", h.CreateSale)
		r.GET("/sale/:id", h.GetSaleByID)
		r.GET("/sale", h.GetSaleList)
		r.PUT("/sale/:id", h.UpdateSale)
		r.DELETE("/sale/:id", h.DeleteSale)

		r.POST("/sale_product", h.CreateSaleProduct)
		r.GET("/sale_product/:id", h.GetSaleProductByID)
		r.GET("/sale_product", h.GetSaleProductList)
		r.PUT("/sale_product/:id", h.UpdateSaleProduct)
		r.DELETE("/sale_product/:id", h.DeleteSaleProduct)

		r.POST("/payment", h.CreatePayment)
		r.GET("/payment/:id", h.GetPaymentByID)
		r.GET("/payment", h.GetPaymentList)
		r.PUT("/payment/:id", h.UpdatePayment)
		r.DELETE("/payment/:id", h.DeletePayment)

		r.POST("/transaction", h.CreateTransaction)
		r.GET("/transaction/:id", h.GetTransactionByID)
		r.GET("/transaction", h.GetTransactionList)
		r.PUT("/transaction/:id", h.UpdateTransaction)
		r.DELETE("/transaction/:id", h.DeleteTransaction)

		r.POST("/shift", h.CreateShift)
		r.GET("/shift/:id", h.GetShiftByID)
		r.GET("/shift", h.GetShiftList)
		r.PUT("/shift/:id", h.UpdateShift)
		r.DELETE("/shift/:id", h.DeleteShift)
	}

	{
		//remainder service
		r.PUT("/do_income/:id", h.DoIncome)

		r.POST("/remainder", h.CreateRemainder)
		r.GET("/remainder/:id", h.GetRemainderByID)
		r.GET("/remainder", h.GetRemainderList)
		r.PUT("/remainder/:id", h.UpdateRemainder)
		r.DELETE("/remainder/:id", h.DeleteRemainder)

		r.POST("/income", h.CreateIncome)
		r.GET("/income/:id", h.GetIncomeByID)
		r.GET("/income", h.GetIncomeList)
		r.PUT("/income/:id", h.UpdateIncome)
		r.DELETE("/income/:id", h.DeleteIncome)

		r.POST("/income_product", h.CreateIncomeProduct)
		r.GET("/income_product/:id", h.GetIncomeProductByID)
		r.GET("/income_product", h.GetIncomeProductList)
		r.PUT("/income_product/:id", h.UpdateIncomeProduct)
		r.DELETE("/income_product/:id", h.DeleteIncomeProduct)
	}

	{
		//organization service
		r.POST("/branch", h.CreateBranch)
		r.GET("/branch/:id", h.GetBranchByID)
		r.GET("/branch", h.GetBranchList)
		r.PUT("/branch/:id", h.UpdateBranch)
		r.DELETE("/branch/:id", h.DeleteBranch)

		r.POST("/market", h.CreateMarket)
		r.GET("/market/:id", h.GetMarketByID)
		r.GET("/market", h.GetMarketList)
		r.PUT("/market/:id", h.UpdateMarket)
		r.DELETE("/market/:id", h.DeleteMarket)

		r.POST("/supplier", h.CreateSupplier)
		r.GET("/supplier/:id", h.GetSupplierByID)
		r.GET("/supplier", h.GetSupplierList)
		r.PUT("/supplier/:id", h.UpdateSupplier)
		r.DELETE("/supplier/:id", h.DeleteSupplier)

		r.POST("/employee", h.CreateEmployee)
		r.GET("/employee/:id", h.GetEmployeeByID)
		r.GET("/employee", h.GetEmployeeList)
		r.PUT("/employee/:id", h.UpdateEmployee)
		r.DELETE("/employee/:id", h.DeleteEmployee)
	}

	{
		// content_service
		r.POST("/brand", h.CreateBrand)
		r.GET("/brand/:id", h.GetBrandByID)
		r.GET("/brand", h.GetBrandList)
		r.PUT("/brand/:id", h.UpdateBrand)
		r.DELETE("/brand/:id", h.DeleteBrand)

		r.POST("/category", h.CreateCategory)
		r.GET("/category/:id", h.GetCategoryByID)
		r.GET("/category", h.GetCategoryList)
		r.PUT("/category/:id", h.UpdateCategory)
		r.DELETE("/category/:id", h.DeleteCategory)

		r.POST("/product", h.CreateProduct)
		r.GET("/product/:id", h.GetProductByID)
		r.GET("/product", h.GetProductList)
		r.PUT("/product/:id", h.UpdateProduct)
		r.DELETE("/product/:id", h.DeleteProduct)
	}

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
