package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateShift godoc
// @ID create_Shift
// @Router /shift [POST]
// @Summary Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param profile body checkout_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShift(c *gin.Context) {
	var Shift checkout_service.CreateShift

	err := c.ShouldBindJSON(&Shift)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Create(
		c.Request.Context(),
		&Shift,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetShiftByID godoc
// @ID get_Shift_by_id
// @Router /shift/{id} [GET]
// @Summary Get Shift By ID
// @Description Get Shift By ID
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=checkout_service.Shift} "ShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftByID(c *gin.Context) {
	ShiftID := c.Param("id")

	if !util.IsValidUUID(ShiftID) {
		h.handlerResponse(c, "Shift id is an invalid uuid", http.StatusBadRequest, "Shift id is an invalid uuid")
		return
	}

	resp, err := h.services.ShiftService().GetByID(
		context.Background(),
		&checkout_service.ShiftPrimaryKey{
			Id: ShiftID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateShift godoc
// @ID update_Shift
// @Router /shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body checkout_service.UpdateShift true "UpdateShiftRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Shift} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShift(c *gin.Context) {
	var updateReq checkout_service.UpdateShift
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Shift id is an invalid uuid", http.StatusBadRequest, "Shift id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteShift godoc
// @ID delete_Shift
// @Router /shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShift(c *gin.Context) {
	ShiftID := c.Param("id")

	if !util.IsValidUUID(ShiftID) {
		h.handlerResponse(c, "Shift id is an invalid uuid", http.StatusBadRequest, "Shift id is an invalid uuid")
		return
	}

	resp, err := h.services.ShiftService().Delete(
		c.Request.Context(),
		&checkout_service.ShiftPrimaryKey{Id: ShiftID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetShiftList godoc
// @ID get_Shift_list
// @Router /shift [GET]
// @Summary Get Shifts List
// @Description Get Shifts List
// @Tags Shift
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=checkout_service.GetListShiftResponse} "GetAllShiftResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.ShiftService().GetList(
		context.Background(),
		&checkout_service.GetListShiftRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
