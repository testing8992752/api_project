package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateBranch godoc
// @ID create_branch
// @Router /branch [POST]
// @Summary Create Branch
// @Description  Create Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateBranchRequest true "CreateBranchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Branch} "GetBranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBranch(c *gin.Context) {

	var branch organization_service.CreateBranchRequest

	err := c.ShouldBindJSON(&branch)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Create(
		c.Request.Context(),
		&branch,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetBranchByID godoc
// @ID get_branch_by_id
// @Router /branch/{id} [GET]
// @Summary Get Branch  By ID
// @Description Get Branch  By ID
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Branch} "BranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchByID(c *gin.Context) {

	branchID := c.Param("id")

	if !util.IsValidUUID(branchID) {
		h.handlerResponse(c, "branch id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid branch id"))
		return
	}

	resp, err := h.services.BranchService().GetByID(
		context.Background(),
		&organization_service.BranchPrimaryKey{
			Id: branchID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id branch resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetBranchList godoc
// @ID get_branch_list
// @Router /branch [GET]
// @Summary Get Branch s List
// @Description  Get Branch s List
// @Tags Branch
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListBranchResponse} "GetAllBranchResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().GetList(
		context.Background(),
		&organization_service.GetListBranchRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateBranch godoc
// @ID update_branch
// @Router /branch/{id} [PUT]
// @Summary Update Branch
// @Description Update Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateBranchRequest true "UpdateBranchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Branch} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBranch(c *gin.Context) {

	var branch organization_service.UpdateBranchRequest

	branch.Id = c.Param("id")

	if !util.IsValidUUID(branch.Id) {
		h.handlerResponse(c, "branch id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid branch id"))
		return
	}

	err := c.ShouldBindJSON(&branch)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Update(
		c.Request.Context(),
		&branch,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// DeleteBranch godoc
// @ID delete_branch
// @Router /branch/{id} [DELETE]
// @Summary Delete Branch
// @Description Delete Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBranch(c *gin.Context) {

	branchId := c.Param("id")

	if !util.IsValidUUID(branchId) {
		h.handlerResponse(c, "branch id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid branch id"))
		return
	}

	resp, err := h.services.BranchService().Delete(
		c.Request.Context(),
		&organization_service.BranchPrimaryKey{Id: branchId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
