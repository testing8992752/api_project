package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreatePayment godoc
// @ID create_Payment
// @Router /payment [POST]
// @Summary Create Payment
// @Description Create Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param profile body checkout_service.CreatePayment true "CreatePaymentRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Payment} "GetPaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreatePayment(c *gin.Context) {
	var Payment checkout_service.CreatePayment

	err := c.ShouldBindJSON(&Payment)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().Create(
		c.Request.Context(),
		&Payment,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetPaymentByID godoc
// @ID get_Payment_by_id
// @Router /payment/{id} [GET]
// @Summary Get Payment By ID
// @Description Get Payment By ID
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=checkout_service.Payment} "PaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentByID(c *gin.Context) {
	PaymentID := c.Param("id")

	if !util.IsValidUUID(PaymentID) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, "Payment id is an invalid uuid")
		return
	}

	resp, err := h.services.PaymentService().GetByID(
		context.Background(),
		&checkout_service.PaymentPrimaryKey{
			Id: PaymentID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdatePayment godoc
// @ID update_Payment
// @Router /payment/{id} [PUT]
// @Summary Update Payment
// @Description Update Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body checkout_service.UpdatePayment true "UpdatePaymentRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Payment} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePayment(c *gin.Context) {
	var updateReq checkout_service.UpdatePayment
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, "Payment id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeletePayment godoc
// @ID delete_Payment
// @Router /payment/{id} [DELETE]
// @Summary Delete Payment
// @Description Delete Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeletePayment(c *gin.Context) {
	PaymentID := c.Param("id")

	if !util.IsValidUUID(PaymentID) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, "Payment id is an invalid uuid")
		return
	}

	resp, err := h.services.PaymentService().Delete(
		c.Request.Context(),
		&checkout_service.PaymentPrimaryKey{Id: PaymentID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetPaymentList godoc
// @ID get_Payment_list
// @Router /payment [GET]
// @Summary Get Payments List
// @Description Get Payments List
// @Tags Payment
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=checkout_service.GetListPaymentResponse} "GetAllPaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.PaymentService().GetList(
		context.Background(),
		&checkout_service.GetListPaymentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
