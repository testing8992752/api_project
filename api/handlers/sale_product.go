package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateSaleProduct godoc
// @ID create_SaleProduct
// @Router /sale_product [POST]
// @Summary Create SaleProduct
// @Description Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param profile body checkout_service.CreateSaleProduct true "CreateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSaleProduct(c *gin.Context) {
	var SaleProduct checkout_service.CreateSaleProduct

	err := c.ShouldBindJSON(&SaleProduct)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Create(
		c.Request.Context(),
		&SaleProduct,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetSaleProductByID godoc
// @ID get_SaleProduct_by_id
// @Router /sale_product/{id} [GET]
// @Summary Get SaleProduct By ID
// @Description Get SaleProduct By ID
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=checkout_service.SaleProduct} "SaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductByID(c *gin.Context) {
	SaleProductID := c.Param("id")

	if !util.IsValidUUID(SaleProductID) {
		h.handlerResponse(c, "SaleProduct id is an invalid uuid", http.StatusBadRequest, "SaleProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductService().GetByID(
		context.Background(),
		&checkout_service.SaleProductPrimaryKey{
			Id: SaleProductID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "get by id", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateSaleProduct godoc
// @ID update_SaleProduct
// @Router /sale_product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body checkout_service.UpdateSaleProduct true "UpdateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.SaleProduct} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSaleProduct(c *gin.Context) {
	var updateReq checkout_service.UpdateSaleProduct
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "SaleProduct id is an invalid uuid", http.StatusBadRequest, "SaleProduct id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSaleProduct godoc
// @ID delete_SaleProduct
// @Router /sale_product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {
	SaleProductID := c.Param("id")

	if !util.IsValidUUID(SaleProductID) {
		h.handlerResponse(c, "SaleProduct id is an invalid uuid", http.StatusBadRequest, "SaleProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductService().Delete(
		c.Request.Context(),
		&checkout_service.SaleProductPrimaryKey{Id: SaleProductID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetSaleProductList godoc
// @ID get_SaleProduct_list
// @Router /sale_product [GET]
// @Summary Get SaleProducts List
// @Description Get SaleProducts List
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=checkout_service.GetListSaleProductResponse} "GetAllSaleProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.SaleProductService().GetList(
		context.Background(),
		&checkout_service.GetListSaleProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
