package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateSupplier godoc
// @ID create_supplier
// @Router /supplier [POST]
// @Summary Create Supplier
// @Description Create Supplier
// @Tags Supplier
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateSupplierRequest true "CreateSupplierRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Supplier} "GetSupplierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSupplier(c *gin.Context) {
	var supplier organization_service.CreateSupplierRequest

	err := c.ShouldBindJSON(&supplier)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SupplierService().Create(
		c.Request.Context(),
		&supplier,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetSupplierByID godoc
// @ID get_supplier_by_id
// @Router /supplier/{id} [GET]
// @Summary Get Supplier By ID
// @Description Get Supplier By ID
// @Tags Supplier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Supplier} "SupplierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSupplierByID(c *gin.Context) {
	supplierID := c.Param("id")

	if !util.IsValidUUID(supplierID) {
		h.handlerResponse(c, "supplier id is an invalid uuid", http.StatusBadRequest, "supplier id is an invalid uuid")
		return
	}

	resp, err := h.services.SupplierService().GetByID(
		context.Background(),
		&organization_service.SupplierPrimaryKey{
			Id: supplierID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateSupplier godoc
// @ID update_supplier
// @Router /supplier/{id} [PUT]
// @Summary Update Supplier
// @Description Update Supplier
// @Tags Supplier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateSupplierRequest true "UpdateSupplierRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Supplier} "Supplier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSupplier(c *gin.Context) {
	var updateReq organization_service.UpdateSupplierRequest
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "supplier id is an invalid uuid", http.StatusBadRequest, "supplier id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SupplierService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSupplier godoc
// @ID delete_supplier
// @Router /supplier/{id} [DELETE]
// @Summary Delete Supplier
// @Description Delete Supplier
// @Tags Supplier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Supplier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSupplier(c *gin.Context) {
	supplierID := c.Param("id")

	if !util.IsValidUUID(supplierID) {
		h.handlerResponse(c, "supplier id is an invalid uuid", http.StatusBadRequest, "supplier id is an invalid uuid")
		return
	}

	resp, err := h.services.SupplierService().Delete(
		c.Request.Context(),
		&organization_service.SupplierPrimaryKey{Id: supplierID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetSupplierList godoc
// @ID get_supplier_list
// @Router /supplier [GET]
// @Summary Get Suppliers List
// @Description Get Suppliers List
// @Tags Supplier
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListSupplierResponse} "GetAllSupplierResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSupplierList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.SupplierService().GetList(
		context.Background(),
		&organization_service.GetListSupplierRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusOK, resp)

}
