package handlers

import (
	"errors"
	"fmt"
	"user_interest/ui_go_api_gateway/api/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/genproto/remainder_service"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// DoIncome godoc
// @ID do_income
// @Router /do_income/{id} [PUT]
// @Summary DoIncome
// @Description DoIncome
// @Tags DoIncome
// @Accept json
// @Produce json
// @Param id path string true "Id"
// @Success 200 {object} http.Response{data=string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DoIncome(c *gin.Context) {

	var (
		income_id = c.Param("id")
	)

	// status check Income
	incomeResp, err := h.services.IncomeService().GetByID(
		c.Request.Context(),
		&remainder_service.IncomePrimaryKey{Id: income_id},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.BadRequest.Code, err.Error())
		return
	} else if incomeResp.Status == "success" {
		h.handlerResponse(c, "error", http.BadRequest.Code, errors.New("status already finished"))
		return
	}

	incomeProductResp, err := h.services.IncomeProductService().GetList(
		c.Request.Context(),
		&remainder_service.GetListIncomeProductRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.BadRequest.Code, err.Error())
		return
	}

	// getall Remainder
	remainderResp, err := h.services.RemainderService().GetList(
		c.Request.Context(),
		&remainder_service.GetListRemainderRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.BadEnvironment.Code, err.Error())
		return
	}

	for _, incomeP := range incomeProductResp.IncomeProducts {

		if incomeP.IncomeId == income_id {
			isHave := false

			for _, remainder := range remainderResp.Remainders {
				if incomeP.ProductId == remainder.ProductId && incomeResp.BranchId == remainder.Branch {
					isHave = true
					// update Remainder if exist
					_, err := h.services.RemainderService().Update(
						c.Request.Context(),
						&remainder_service.UpdateRemainder{
							Id:          remainder.Id,
							Branch:      remainder.Branch,
							Brand:       remainder.Brand,
							CategoryId:  remainder.CategoryId,
							ProductId:   remainder.ProductId,
							Barcode:     remainder.Barcode,
							IncomePrice: incomeP.IncomePrice,
							Quantity:    incomeP.Quantity + remainder.Quantity,
						},
					)
					if err != nil {
						fmt.Println("__________________BUG_____1111")
						h.handlerResponse(c, "error", http.BadRequest.Code, err.Error())
						return
					}
				}
			}
			// insert Remainder if not exist
			if !isHave {
				_, err := h.services.RemainderService().Create(
					c.Request.Context(),
					&remainder_service.CreateRemainder{
						Branch:      incomeResp.BranchId,
						Brand:       incomeP.Brand,
						CategoryId:  incomeP.CategoryId,
						ProductId:   incomeP.ProductId,
						Barcode:     incomeP.Barcode,
						IncomePrice: incomeP.IncomePrice,
						Quantity:    incomeP.Quantity,
					},
				)
				if err != nil {
					fmt.Println("__________________BUG___22222")
					h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
					return
				}
			}
		}
	}

	// update status Remainder
	_, err = h.services.IncomeService().Update(
		c.Request.Context(),
		&remainder_service.UpdateIncome{
			Id:         incomeResp.Id,
			BranchId:   incomeResp.BranchId,
			SupplierId: incomeResp.SupplierId,
			Date:       incomeResp.Date,
			Status:     "success",
		},
	)
	if err != nil {
		fmt.Println("__________________BUG____333333")
		h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
		return
	}

	h.handlerResponse(c, "success", http.OK.Code, nil)
}

// @Security ApiKeyAuth
// DoSale godoc
// @ID do_sale
// @Router /do_sale/{id} [PUT]
// @Summary DoSale
// @Description DoSale
// @Tags DoSale
// @Accept json
// @Produce json
// @Param id path string true "Id"
// @Success 200 {object} http.Response{data=string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DoSale(c *gin.Context) {

	var (
		sale_id = c.Param("id")
	)

	// status check Sale
	saleResp, err := h.services.SaleService().GetByID(
		c.Request.Context(),
		&checkout_service.SalePrimaryKey{
			Id: sale_id,
		},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.BadRequest.Code, err.Error())
		return
	} else if saleResp.Status == "finished" {
		h.handlerResponse(c, "error", http.BadRequest.Code, errors.New("status already finished"))
		return
	}

	// getall SaleProduct
	saleProductResp, err := h.services.SaleProductService().GetList(
		c.Request.Context(),
		&checkout_service.GetListSaleProductRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
		return
	}

	// getall Remainder
	remainderResp, err := h.services.RemainderService().GetList(
		c.Request.Context(),
		&remainder_service.GetListRemainderRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
		return
	}

	// check enough quantity Remainder's
	for _, saleP := range saleProductResp.SaleProducts {
		if saleP.SaleId == sale_id {
			for _, remainder := range remainderResp.Remainders {
				if remainder.ProductId == saleP.ProductId && remainder.Branch == saleResp.BranchId {
					// if not enough quantity return error
					if remainder.Quantity-saleP.Quantity < 0 {
						h.handlerResponse(c, "error", http.BadRequest.Code, errors.New("not enough quantity in stock"))
						return

					}
				}
			}
		}
	}

	// update quantity Remainders
	for _, saleP := range saleProductResp.SaleProducts {
		if saleP.SaleId == sale_id {
			for _, remainder := range remainderResp.Remainders {
				if remainder.ProductId == saleP.ProductId && remainder.Branch == saleResp.BranchId {
					_, err := h.services.RemainderService().Update(
						c.Request.Context(),
						&remainder_service.UpdateRemainder{
							Id:          remainder.Id,
							Branch:      remainder.Branch,
							Brand:       remainder.Brand,
							CategoryId:  remainder.CategoryId,
							ProductId:   remainder.ProductId,
							Barcode:     remainder.Barcode,
							IncomePrice: remainder.IncomePrice,
							Quantity:    remainder.Quantity - saleP.Quantity,
						},
					)
					if err != nil {
						h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
						return
					}
				}
			}
		}
	}

	// update status Sale
	_, err = h.services.SaleService().Update(
		c.Request.Context(),
		&checkout_service.UpdateSale{
			Id:          saleResp.Id,
			IncrementId: saleResp.IncrementId,
			BranchId:    saleResp.BranchId,
			SalePointId: saleResp.SalePointId,
			ShiftId:     saleResp.ShiftId,
			EmployeeId:  saleResp.EmployeeId,
			Barcode:     saleResp.Barcode,
			Status:      "finished",
		},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.InternalServerError.Code, err.Error())
		return
	}

	h.handlerResponse(c, "success", http.OK.Code, nil)
}
