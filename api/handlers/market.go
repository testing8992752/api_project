package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateMarket godoc
// @ID create_market
// @Router /market [POST]
// @Summary Create Market
// @Description  Create Market
// @Tags Market
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateMarket true "CreateMarketRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Market} "GetMarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateMarket(c *gin.Context) {

	var market organization_service.CreateMarket

	err := c.ShouldBindJSON(&market)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.MarketService().Create(
		c.Request.Context(),
		&market,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetMarketByID godoc
// @ID get_market_by_id
// @Router /market/{id} [GET]
// @Summary Get Market  By ID
// @Description Get Market  By ID
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Market} "MarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketByID(c *gin.Context) {

	marketID := c.Param("id")

	if !util.IsValidUUID(marketID) {
		h.handlerResponse(c, "market id is an invalid uuid", http.StatusBadRequest, "market id is an invalid uuid")
		return
	}

	resp, err := h.services.MarketService().GetByID(
		context.Background(),
		&organization_service.MarketPrimaryKey{
			Id: marketID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "get by id ", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetMarketList godoc
// @ID get_market_list
// @Router /market [GET]
// @Summary Get Market s List
// @Description  Get Market s List
// @Tags Market
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListMarketResponse} "GetAllMarketResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.MarketService().GetList(
		context.Background(),
		&organization_service.GetListMarketRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateMarket godoc
// @ID update_market
// @Router /market/{id} [PUT]
// @Summary Update Market
// @Description Update Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateMarket true "UpdateMarketRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Market} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateMarket(c *gin.Context) {

	var market organization_service.UpdateMarket

	market.Id = c.Param("id")

	if !util.IsValidUUID(market.Id) {
		h.handlerResponse(c, "market id is an invalid uuid", http.StatusBadRequest, "market id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&market)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.MarketService().Update(
		c.Request.Context(),
		&market,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteMarket godoc
// @ID delete_market
// @Router /market/{id} [DELETE]
// @Summary Delete Market
// @Description Delete Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteMarket(c *gin.Context) {

	marketId := c.Param("id")

	if !util.IsValidUUID(marketId) {
		h.handlerResponse(c, "market id is an invalid uuid", http.StatusBadRequest, "market id is an invalid uuid")
		return
	}

	resp, err := h.services.MarketService().Delete(
		c.Request.Context(),
		&organization_service.MarketPrimaryKey{Id: marketId},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
