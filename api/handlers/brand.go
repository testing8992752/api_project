package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/content_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateBrand godoc
// @ID create_brand
// @Router /brand [POST]
// @Summary Create Brand
// @Description Create Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param profile body content_service.CreateBrand true "CreateBrandRequestBody"
// @Success 200 {object} http.Response{data=content_service.Brand} "GetBrandBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBrand(c *gin.Context) {
	var brand content_service.CreateBrand

	err := c.ShouldBindJSON(&brand)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BrandService().Create(
		c.Request.Context(),
		&brand,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetBrandByID godoc
// @ID get_brand_by_id
// @Router /brand/{id} [GET]
// @Summary Get Brand By ID
// @Description Get Brand By ID
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=content_service.Brand} "BrandBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandByID(c *gin.Context) {
	brandID := c.Param("id")

	if !util.IsValidUUID(brandID) {
		h.handlerResponse(c, "branch id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid brand id"))
		return
	}

	resp, err := h.services.BrandService().GetByID(
		context.Background(),
		&content_service.BrandPrimaryKey{
			Id: brandID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id branch resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateBrand godoc
// @ID update_brand
// @Router /brand/{id} [PUT]
// @Summary Update Brand
// @Description Update Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body content_service.UpdateBrand true "UpdateBrandRequestBody"
// @Success 200 {object} http.Response{data=content_service.Brand} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBrand(c *gin.Context) {
	var updateReq content_service.UpdateBrand
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "brand id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid branch id"))
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BrandService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// DeleteBrand godoc
// @ID delete_brand
// @Router /brand/{id} [DELETE]
// @Summary Delete Brand
// @Description Delete Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBrand(c *gin.Context) {
	brandID := c.Param("id")

	if !util.IsValidUUID(brandID) {
		h.handlerResponse(c, "brand id is an invalid uuid", http.StatusBadRequest, errors.New("Invalid branch id"))
		return
	}

	resp, err := h.services.BrandService().Delete(
		c.Request.Context(),
		&content_service.BrandPrimaryKey{Id: brandID},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetBrandList godoc
// @ID get_brand_list
// @Router /brand [GET]
// @Summary Get Brands List
// @Description Get Brands List
// @Tags Brand
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=content_service.GetListBrandResponse} "GetAllBrandResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BrandService().GetList(
		context.Background(),
		&content_service.GetListBrandRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}
