package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateTransaction godoc
// @ID create_Transaction
// @Router /transaction [POST]
// @Summary Create Transaction
// @Description Create Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param profile body checkout_service.CreateTransaction true "CreateTransactionRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Transaction} "GetTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTransaction(c *gin.Context) {
	var Transaction checkout_service.CreateTransaction

	err := c.ShouldBindJSON(&Transaction)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Create(
		c.Request.Context(),
		&Transaction,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetTransactionByID godoc
// @ID get_Transaction_by_id
// @Router /transaction/{id} [GET]
// @Summary Get Transaction By ID
// @Description Get Transaction By ID
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=checkout_service.Transaction} "TransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionByID(c *gin.Context) {
	TransactionID := c.Param("id")

	if !util.IsValidUUID(TransactionID) {
		h.handlerResponse(c, "Transaction id is an invalid uuid", http.StatusBadRequest, "Transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().GetByID(
		context.Background(),
		&checkout_service.TransactionPrimaryKey{
			Id: TransactionID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateTransaction godoc
// @ID update_Transaction
// @Router /transaction/{id} [PUT]
// @Summary Update Transaction
// @Description Update Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body checkout_service.UpdateTransaction true "UpdateTransactionRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Transaction} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTransaction(c *gin.Context) {
	var updateReq checkout_service.UpdateTransaction
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Transaction id is an invalid uuid", http.StatusBadRequest, "Transaction id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteTransaction godoc
// @ID delete_Transaction
// @Router /transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTransaction(c *gin.Context) {
	TransactionID := c.Param("id")

	if !util.IsValidUUID(TransactionID) {
		h.handlerResponse(c, "Transaction id is an invalid uuid", http.StatusBadRequest, "Transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().Delete(
		c.Request.Context(),
		&checkout_service.TransactionPrimaryKey{Id: TransactionID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetTransactionList godoc
// @ID get_Transaction_list
// @Router /transaction [GET]
// @Summary Get Transactions List
// @Description Get Transactions List
// @Tags Transaction
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=checkout_service.GetListTransactionResponse} "GetAllTransactionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.TransactionService().GetList(
		context.Background(),
		&checkout_service.GetListTransactionRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "list", http.StatusOK, resp)

}
