package handlers

import (
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// OpenShift godoc
// @ID OpenShift
// @Router /open-shift [POST]
// @Summary OpenShift
// @Description OpenShift
// @Tags OpenShift
// @Accept json
// @Procedure json
// @Param OpenShift body checkout_service.CreateShift true "OpenShiftRequest"
// @Success 200 {object}  http.Response{data=string} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object}  http.Response{data=string} "Server error"
func (h *Handler) OpenShift(c *gin.Context) {
	var Shift checkout_service.CreateShift

	err := c.ShouldBind(&Shift)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	branch, err := h.services.ShiftService().GetList(c, &checkout_service.GetListShiftRequest{
		Search: Shift.BranchId,
	})
	if err != nil {
		h.handlerResponse(c, "error while getting list from shift", http.StatusBadRequest, err.Error())
		return
	}

	hasOpenShift := false

	for _, value := range branch.Shifts {
		if value.Status == "Open" {
			hasOpenShift = true
			break
		}
	}

	if hasOpenShift {
		h.handlerResponse(c, "У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его!", http.StatusBadRequest, errors.New("У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его!").Error())
		return
	}

	resp, err := h.services.ShiftService().Create(c, &Shift)
	if err != nil {
		h.handlerResponse(c, "error while creating transaction", http.StatusBadRequest, err.Error())
		return
	}

	_, err = h.services.TransactionService().Create(c, &checkout_service.CreateTransaction{
		Cash:    0,
		Uzcard:  0,
		Payme:   0,
		Click:   0,
		Humo:    0,
		ShiftId: resp.Id,
	})
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "created transaction", http.StatusOK, resp)
}
