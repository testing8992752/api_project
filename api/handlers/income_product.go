package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/remainder_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateIncomeProduct godoc
// @ID create_IncomeProduct
// @Router /income_product [POST]
// @Summary Create IncomeProduct
// @Description Create IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Produce json
// @Param profile body remainder_service.CreateIncomeProduct true "CreateIncomeProductRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.IncomeProduct} "GetIncomeProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateIncomeProduct(c *gin.Context) {
	var IncomeProduct remainder_service.CreateIncomeProduct

	err := c.ShouldBindJSON(&IncomeProduct)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeProductService().Create(
		c.Request.Context(),
		&IncomeProduct,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth// @Security ApiKeyAuth
// GetIncomeProductByID godoc
// @ID get_IncomeProduct_by_id
// @Router /income_product/{id} [GET]
// @Summary Get IncomeProduct By ID
// @Description Get IncomeProduct By ID
// @Tags IncomeProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=remainder_service.IncomeProduct} "IncomeProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetIncomeProductByID(c *gin.Context) {
	IncomeProductID := c.Param("id")

	if !util.IsValidUUID(IncomeProductID) {
		h.handlerResponse(c, "IncomeProduct id is an invalid uuid", http.StatusBadRequest, "IncomeProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.IncomeProductService().GetByID(
		context.Background(),
		&remainder_service.IncomeProductPrimaryKey{
			Id: IncomeProductID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "get by id", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateIncomeProduct godoc
// @ID update_IncomeProduct
// @Router /income_product/{id} [PUT]
// @Summary Update IncomeProduct
// @Description Update IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body remainder_service.UpdateIncomeProduct true "UpdateIncomeProductRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.IncomeProduct} "IncomeProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateIncomeProduct(c *gin.Context) {
	var updateReq remainder_service.UpdateIncomeProduct
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "IncomeProduct id is an invalid uuid", http.StatusBadRequest, "IncomeProduct id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeProductService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteIncomeProduct godoc
// @ID delete_IncomeProduct
// @Router /income_product/{id} [DELETE]
// @Summary Delete IncomeProduct
// @Description Delete IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "IncomeProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteIncomeProduct(c *gin.Context) {
	IncomeProductID := c.Param("id")

	if !util.IsValidUUID(IncomeProductID) {
		h.handlerResponse(c, "IncomeProduct id is an invalid uuid", http.StatusBadRequest, "IncomeProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.IncomeProductService().Delete(
		c.Request.Context(),
		&remainder_service.IncomeProductPrimaryKey{Id: IncomeProductID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetIncomeProductList godoc
// @ID get_IncomeProduct_list
// @Router /income_product [GET]
// @Summary Get IncomeProducts List
// @Description Get IncomeProducts List
// @Tags IncomeProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=remainder_service.GetListIncomeProductResponse} "GetAllIncomeProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetIncomeProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.IncomeProductService().GetList(
		context.Background(),
		&remainder_service.GetListIncomeProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
