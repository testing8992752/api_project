package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateEmployee godoc
// @ID create_employee
// @Router /employee [POST]
// @Summary Create Employee
// @Description Create Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateEmployee true "CreateEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Employee} "GetEmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateEmployee(c *gin.Context) {
	var employee organization_service.CreateEmployee

	err := c.ShouldBindJSON(&employee)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().Create(
		c.Request.Context(),
		&employee,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetEmployeeByID godoc
// @ID get_employee_by_id
// @Router /employee/{id} [GET]
// @Summary Get Employee By ID
// @Description Get Employee By ID
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Employee} "EmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEmployeeByID(c *gin.Context) {
	employeeID := c.Param("id")

	if !util.IsValidUUID(employeeID) {
		h.handlerResponse(c, "employee id is an invalid uuid", http.StatusBadRequest, errors.New("employee id is an invalid uuid"))
		return
	}

	resp, err := h.services.EmployeeService().GetByID(
		context.Background(),
		&organization_service.EmployeePrimaryKey{
			Id: employeeID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "get by id", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateEmployee godoc
// @ID update_employee
// @Router /employee/{id} [PUT]
// @Summary Update Employee
// @Description Update Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateEmployee true "UpdateEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Employee} "Employee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateEmployee(c *gin.Context) {
	var updateReq organization_service.UpdateEmployee
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "employee id is an invalid uuid", http.StatusBadRequest, "employee id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteEmployee godoc
// @ID delete_employee
// @Router /employee/{id} [DELETE]
// @Summary Delete Employee
// @Description Delete Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Employee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteEmployee(c *gin.Context) {
	employeeID := c.Param("id")

	if !util.IsValidUUID(employeeID) {
		h.handlerResponse(c, "employee id is an invalid uuid", http.StatusBadRequest, "employee id is an invalid uuid")
		return
	}

	resp, err := h.services.EmployeeService().Delete(
		c.Request.Context(),
		&organization_service.EmployeePrimaryKey{Id: employeeID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetEmployeeList godoc
// @ID get_employee_list
// @Router /employee [GET]
// @Summary Get Employees List
// @Description Get Employees List
// @Tags Employee
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListEmployeeResponse} "GetAllEmployeeResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEmployeeList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.EmployeeService().GetList(
		context.Background(),
		&organization_service.GetListEmployeeRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
