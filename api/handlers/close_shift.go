package handlers

import (
	"net/http"
	"time"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CloseShift godoc
// @ID CloseShift
// @Router /close-shift/{id} [POST]
// @Summary CloseShift
// @Description CloseShift
// @Tags CloseShift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object}  http.Response{data=string} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object}  http.Response{data=string} "Server error"
func (h *Handler) CloseShift(ctx *gin.Context) {
	ShiftId := ctx.Param("id")

	resp, err := h.services.ShiftService().GetByID(ctx, &checkout_service.ShiftPrimaryKey{Id: ShiftId})
	if err != nil {
		h.handlerResponse(ctx, "error", http.StatusBadRequest, err.Error())
		return
	}

	_, err = h.services.ShiftService().Update(ctx, &checkout_service.UpdateShift{
		Id:         resp.Id,
		BranchId:   resp.BranchId,
		UserId:     resp.UserId,
		SalePoint:  resp.SalePoint,
		OpenShift:  resp.OpenShift,
		CloseShift: time.Now().Format("2006-01-02 15:04:05"),
		Status:     "closed",
	})
	if err != nil {
		h.handlerResponse(ctx,"error while closing shift" , http.StatusBadRequest, err.Error())
		return
	}
	h.handlerResponse(ctx, "closed", http.StatusNoContent, resp)
}
