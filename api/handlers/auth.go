package handlers

import (
	"errors"
	"net/http"
	"time"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/pkg/helper"

	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param login body organization_service.EmployeeLogin true "LoginRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Login(ctx *gin.Context) {
	var login organization_service.EmployeeLogin

	err := ctx.ShouldBindJSON(&login)
	if err != nil {
		h.handlerResponse(ctx, "create staff", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().GetByID(ctx, &organization_service.EmployeePrimaryKey{Login: login.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			h.handlerResponse(ctx, "employee does not exist", http.StatusBadRequest, "Employee does not exist")
			return
		}
		h.handlerResponse(ctx, "error while  employee get by id", http.StatusInternalServerError, err.Error())
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(resp.Password), []byte(login.Password))
	if err != nil {
		h.handlerResponse(ctx, "Compare Password", http.StatusInternalServerError, err.Error())
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"employee_id": resp.Id,
	}, time.Hour*360, h.cfg.SecretKey)
	if err != nil {
		h.handlerResponse(ctx, "Generate JWT", http.StatusInternalServerError, err.Error())
		return
	}

	ctx.SetCookie("SESSTOKEN", token, 1000, "/", "localhost", false, true)
	h.handlerResponse(ctx, "token", http.StatusAccepted, token)
}

// Register godoc
// @ID Register
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body organization_service.CreateEmployee true "RegisterRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Register(ctx *gin.Context) {
	var createEmployee organization_service.CreateEmployee

	err := ctx.ShouldBindJSON(&createEmployee)
	if err != nil {
		h.handlerResponse(ctx, "error employee should bind json", http.StatusBadRequest, err.Error())
		return
	}

	if len(createEmployee.Password) < 8 {
		h.handlerResponse(ctx, "Password should include more than 8 elements", http.StatusBadRequest, errors.New("Password length should include more than 8 elements"))
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(createEmployee.Password), bcrypt.DefaultCost)
	if err != nil {
		h.handlerResponse(ctx, "Error hashing password", http.StatusInternalServerError, err.Error())
		return
	}

	createEmployee.Password = string(hashedPassword)
	_, err = h.services.EmployeeService().GetByID(ctx, &organization_service.EmployeePrimaryKey{
		Login: createEmployee.Login,
	})

	if err != nil {
		if err.Error() == "rpc error: code = InvalidArgument desc = no rows in result set" {
			resp, err := h.services.EmployeeService().Create(ctx, &createEmployee)
			if err != nil {
				h.handlerResponse(ctx, "error while creating employee", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(ctx, "resp", http.StatusCreated, resp)
			return
		} else {
			h.handlerResponse(ctx, "error while getting by id employee", http.StatusBadRequest, err.Error())
			return
		}
	} else if err == nil {
		h.handlerResponse(ctx, "employee already exists", http.StatusBadRequest, nil)
		return
	}
}
