package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/remainder_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateRemainder godoc
// @ID create_Remainder
// @Router /remainder [POST]
// @Summary Create Remainder
// @Description Create Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param profile body remainder_service.CreateRemainder true "CreateRemainderRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.Remainder} "GetRemainderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateRemainder(c *gin.Context) {
	var remainder remainder_service.CreateRemainder

	err := c.ShouldBindJSON(&remainder)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.RemainderService().Create(
		c.Request.Context(),
		&remainder,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetRemainderByID godoc
// @ID get_Remainder_by_id
// @Router /remainder/{id} [GET]
// @Summary Get Remainder By ID
// @Description Get Remainder By ID
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=remainder_service.Remainder} "RemainderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRemainderByID(c *gin.Context) {
	RemainderID := c.Param("id")

	if !util.IsValidUUID(RemainderID) {
		h.handlerResponse(c, "Remainder id is an invalid uuid", http.StatusBadRequest, "Remainder id is an invalid uuid")
		return
	}

	resp, err := h.services.RemainderService().GetByID(
		context.Background(),
		&remainder_service.RemainderPrimaryKey{
			Id: RemainderID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateRemainder godoc
// @ID update_Remainder
// @Router /remainder/{id} [PUT]
// @Summary Update Remainder
// @Description Update Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body remainder_service.UpdateRemainder true "UpdateRemainderRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.Remainder} "Remainder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateRemainder(c *gin.Context) {
	var updateReq remainder_service.UpdateRemainder
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Remainder id is an invalid uuid", http.StatusBadRequest, "Remainder id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.RemainderService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteRemainder godoc
// @ID delete_Remainder
// @Router /remainder/{id} [DELETE]
// @Summary Delete Remainder
// @Description Delete Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Remainder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteRemainder(c *gin.Context) {
	RemainderID := c.Param("id")

	if !util.IsValidUUID(RemainderID) {
		h.handlerResponse(c, "Remainder id is an invalid uuid", http.StatusBadRequest, "Remainder id is an invalid uuid")
		return
	}

	resp, err := h.services.RemainderService().Delete(
		c.Request.Context(),
		&remainder_service.RemainderPrimaryKey{Id: RemainderID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetRemainderList godoc
// @ID get_Remainder_list
// @Router /remainder [GET]
// @Summary Get Remainders List
// @Description Get Remainders List
// @Tags Remainder
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=remainder_service.GetListRemainderResponse} "GetAllRemainderResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRemainderList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.RemainderService().GetList(
		context.Background(),
		&remainder_service.GetListRemainderRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
