package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateSale godoc
// @ID create_Sale
// @Router /sale [POST]
// @Summary Create Sale
// @Description Create Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param profile body checkout_service.CreateSale true "CreateSaleRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Sale} "GetSaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSale(c *gin.Context) {
	var Sale checkout_service.CreateSale

	err := c.ShouldBindJSON(&Sale)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Create(
		c.Request.Context(),
		&Sale,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetSaleByID godoc
// @ID get_Sale_by_id
// @Router /sale/{id} [GET]
// @Summary Get Sale By ID
// @Description Get Sale By ID
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=checkout_service.Sale} "SaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleByID(c *gin.Context) {
	SaleID := c.Param("id")

	if !util.IsValidUUID(SaleID) {
		h.handlerResponse(c, "Sale id is an invalid uuid", http.StatusBadRequest, "Sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().GetByID(
		context.Background(),
		&checkout_service.SalePrimaryKey{
			Id: SaleID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateSale godoc
// @ID update_Sale
// @Router /sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body checkout_service.UpdateSale true "UpdateSaleRequestBody"
// @Success 200 {object} http.Response{data=checkout_service.Sale} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSale(c *gin.Context) {
	var updateReq checkout_service.UpdateSale
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Sale id is an invalid uuid", http.StatusBadRequest, "Sale id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSale godoc
// @ID delete_Sale
// @Router /sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSale(c *gin.Context) {
	SaleID := c.Param("id")

	if !util.IsValidUUID(SaleID) {
		h.handlerResponse(c, "Sale id is an invalid uuid", http.StatusBadRequest, "Sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().Delete(
		c.Request.Context(),
		&checkout_service.SalePrimaryKey{Id: SaleID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetSaleList godoc
// @ID get_Sale_list
// @Router /sale [GET]
// @Summary Get Sales List
// @Description Get Sales List
// @Tags Sale
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=checkout_service.GetListSaleResponse} "GetAllSaleResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.SaleService().GetList(
		context.Background(),
		&checkout_service.GetListSaleRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "sale list", http.StatusOK, resp)

}
