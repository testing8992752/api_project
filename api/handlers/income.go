package handlers

import (
	"context"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/remainder_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateIncome godoc
// @ID create_Income
// @Router /income [POST]
// @Summary Create Income
// @Description Create Income
// @Tags Income
// @Accept json
// @Produce json
// @Param profile body remainder_service.CreateIncome true "CreateIncomeRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.Income} "GetIncomeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateIncome(c *gin.Context) {
	var Income remainder_service.CreateIncome

	err := c.ShouldBindJSON(&Income)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeService().Create(
		c.Request.Context(),
		&Income,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetIncomeByID godoc
// @ID get_Income_by_id
// @Router /income/{id} [GET]
// @Summary Get Income By ID
// @Description Get Income By ID
// @Tags Income
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=remainder_service.Income} "IncomeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetIncomeByID(c *gin.Context) {
	IncomeID := c.Param("id")

	if !util.IsValidUUID(IncomeID) {
		h.handlerResponse(c, "Income id is an invalid uuid", http.StatusBadRequest, "Income id is an invalid uuid")
		return
	}

	resp, err := h.services.IncomeService().GetByID(
		context.Background(),
		&remainder_service.IncomePrimaryKey{
			Id: IncomeID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// UpdateIncome godoc
// @ID update_Income
// @Router /income/{id} [PUT]
// @Summary Update Income
// @Description Update Income
// @Tags Income
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body remainder_service.UpdateIncome true "UpdateIncomeRequestBody"
// @Success 200 {object} http.Response{data=remainder_service.Income} "Income data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateIncome(c *gin.Context) {
	var updateReq remainder_service.UpdateIncome
	updateReq.Id = c.Param("id")

	if !util.IsValidUUID(updateReq.Id) {
		h.handlerResponse(c, "Income id is an invalid uuid", http.StatusBadRequest, "Income id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&updateReq)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeService().Update(
		c.Request.Context(),
		&updateReq,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteIncome godoc
// @ID delete_Income
// @Router /income/{id} [DELETE]
// @Summary Delete Income
// @Description Delete Income
// @Tags Income
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Income data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteIncome(c *gin.Context) {
	IncomeID := c.Param("id")

	if !util.IsValidUUID(IncomeID) {
		h.handlerResponse(c, "Income id is an invalid uuid", http.StatusBadRequest, "Income id is an invalid uuid")
		return
	}

	resp, err := h.services.IncomeService().Delete(
		c.Request.Context(),
		&remainder_service.IncomePrimaryKey{Id: IncomeID},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}

// @Security ApiKeyAuth
// GetIncomeList godoc
// @ID get_Income_list
// @Router /income [GET]
// @Summary Get Incomes List
// @Description Get Incomes List
// @Tags Income
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=remainder_service.GetListIncomeResponse} "GetAllIncomeResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetIncomeList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())

		return
	}

	resp, err := h.services.IncomeService().GetList(
		context.Background(),
		&remainder_service.GetListIncomeRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)

}
