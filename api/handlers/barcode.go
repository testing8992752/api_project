package handlers

import (
	"errors"
	"fmt"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/genproto/content_service"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// @Security ApiKeyAuth
// DoIncome godoc
// @ID scan_barcode
// @Router /barcode/{sale_id}/{barcode}/{quantity} [POST]
// @Summary Scan Barcode
// @Description Scan Barcode
// @Tags Scan Barcode
// @Accept json
// @Produce json
// @Param sale_id path string true "Sale Id"
// @Param barcode path string true "Barcode"
// @Param quantity path int false "Quantity"
// @Success 200 {object} http.Response{data=string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) ScanBarcode(c *gin.Context) {

	var (
		barcode  = c.Param("barcode")
		sale_id  = c.Param("sale_id")
		quantity = cast.ToInt64(c.Param("quantity"))
	)

	if quantity == 0 {
		quantity = 1
	}

	// check Sale status
	saleResp, err := h.services.SaleService().GetByID(
		c.Request.Context(),
		&checkout_service.SalePrimaryKey{Id: sale_id},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	} else if saleResp.Status == "finished" {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("sale status already finished"))
		return
	}

	// check is have SaleProduct
	salePResp, err := h.services.SaleProductService().GetList(
		c.Request.Context(),
		&checkout_service.GetListSaleProductRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	// update SelProduct if exist
	for _, product := range salePResp.SaleProducts {
		if product.SaleId == sale_id && product.Barcode == barcode {
			_, err := h.services.SaleProductService().Update(
				c.Request.Context(),
				&checkout_service.UpdateSaleProduct{
					Id:                product.Id,
					SaleId:            product.SaleId,
					CategoryId:        product.CategoryId,
					ProductId:         product.Id,
					Barcode:           product.Barcode,
					RemainingQuantity: product.Quantity + quantity,
					Quantity:          product.Quantity + quantity,
					AllowDiscount:     product.AllowDiscount,
					DiscountType:      product.DiscountType,
					DiscountPrice:     product.Price + product.Price/10,
					Price:             product.Price,
				},
			)
			if err != nil {
				h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(c, "success", http.StatusOK, nil)
			return
		}
	}

	// getall Product
	productResp, err := h.services.ProductService().GetList(
		c.Request.Context(),
		&content_service.GetListProductRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	// create SaleProduct
	for _, product := range productResp.Products {
		if product.Barcode == barcode {
			_, err = h.services.SaleProductService().Create(
				c.Request.Context(),
				&checkout_service.CreateSaleProduct{
					SaleId:            sale_id,
					CategoryId:        product.CategoryId,
					ProductId:         product.Id,
					Barcode:           product.Barcode,
					RemainingQuantity: quantity,
					Quantity:          quantity,
					AllowDiscount:     true,
					DiscountType:      "persentage",
					DiscountPrice:     product.Price * float32(quantity) / 10,
					Price:             product.Price,
				},
			)
			if err != nil {
				fmt.Println("________________BUG")
				h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
				return
			}
			break
		}
	}
	h.handlerResponse(c, "succes", http.StatusOK, err.Error())
}
