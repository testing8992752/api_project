package services

import (
	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/auth_service"
	"user_interest/ui_go_api_gateway/genproto/checkout_service"
	"user_interest/ui_go_api_gateway/genproto/content_service"
	"user_interest/ui_go_api_gateway/genproto/organization_service"
	"user_interest/ui_go_api_gateway/genproto/remainder_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// organization service
	MarketService() organization_service.MarketServiceClient
	BranchService() organization_service.BranchServiceClient
	EmployeeService() organization_service.EmployeeServiceClient
	SupplierService() organization_service.SupplierServiceClient

	// content service
	BrandService() content_service.BrandServiceClient
	CategoryService() content_service.CategoryServiceClient
	ProductService() content_service.ProductServiceClient

	// remainder service
	RemainderService() remainder_service.RemainderServiceClient
	IncomeService() remainder_service.IncomeServiceClient
	IncomeProductService() remainder_service.IncomeProductServiceClient

	// checkout service
	ShiftService() checkout_service.ShiftServiceClient
	SaleService() checkout_service.SaleServiceClient
	SaleProductService() checkout_service.SaleProductServiceClient
	TransactionService() checkout_service.TransactionServiceClient
	PaymentService() checkout_service.PaymentServiceClient

	//auth_service
	AuthUserService() auth_service.UserServiceClient
	SessionService() auth_service.SessionServiceClient
}

type grpcClients struct {
	// organizations service
	marketService   organization_service.MarketServiceClient
	branchService   organization_service.BranchServiceClient
	employeeService organization_service.EmployeeServiceClient
	supplierService organization_service.SupplierServiceClient

	// content service
	brandService    content_service.BrandServiceClient
	categoryService content_service.CategoryServiceClient
	productService  content_service.ProductServiceClient

	// remainder service
	remainderService     remainder_service.RemainderServiceClient
	incomeService        remainder_service.IncomeServiceClient
	incomeProductService remainder_service.IncomeProductServiceClient

	// checkout service
	shiftService       checkout_service.ShiftServiceClient
	saleService        checkout_service.SaleServiceClient
	saleProductService checkout_service.SaleProductServiceClient
	transactionService checkout_service.TransactionServiceClient
	paymentService     checkout_service.PaymentServiceClient

	//auth_service
	authUserService auth_service.UserServiceClient
	sessionService  auth_service.SessionServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Organization Service...
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	//content _service
	connContentService, err := grpc.Dial(
		cfg.ContentServiceHost+cfg.ContentGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	//remainder service...
	connRemainderService, err := grpc.Dial(
		cfg.RemainderServiceHost+cfg.RemainderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	//checkout service
	connCheckoutService, err := grpc.Dial(
		cfg.CheckoutServiceHost+cfg.CheckoutGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Auth Service...
	connAuthService, err := grpc.Dial(
		cfg.AuthServiceHost+cfg.AuthGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		//organization service
		marketService:   organization_service.NewMarketServiceClient(connOrganizationService),
		branchService:   organization_service.NewBranchServiceClient(connOrganizationService),
		employeeService: organization_service.NewEmployeeServiceClient(connOrganizationService),
		supplierService: organization_service.NewSupplierServiceClient(connOrganizationService),

		// content service
		brandService:    content_service.NewBrandServiceClient(connContentService),
		categoryService: content_service.NewCategoryServiceClient(connContentService),
		productService:  content_service.NewProductServiceClient(connContentService),

		// remainder service
		incomeService:        remainder_service.NewIncomeServiceClient(connRemainderService),
		incomeProductService: remainder_service.NewIncomeProductServiceClient(connRemainderService),
		remainderService:     remainder_service.NewRemainderServiceClient(connRemainderService),

		// checkout service
		shiftService:       checkout_service.NewShiftServiceClient(connCheckoutService),
		saleService:        checkout_service.NewSaleServiceClient(connCheckoutService),
		saleProductService: checkout_service.NewSaleProductServiceClient(connCheckoutService),
		transactionService: checkout_service.NewTransactionServiceClient(connCheckoutService),
		paymentService:     checkout_service.NewPaymentServiceClient(connCheckoutService),

		//auth_service
		authUserService: auth_service.NewUserServiceClient(connAuthService),
		sessionService:  auth_service.NewSessionServiceClient(connAuthService),
	}, nil
}

// organization service methods
func (g *grpcClients) MarketService() organization_service.MarketServiceClient {
	return g.marketService
}

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) EmployeeService() organization_service.EmployeeServiceClient {
	return g.employeeService
}

func (g *grpcClients) SupplierService() organization_service.SupplierServiceClient {
	return g.supplierService
}

// content service methods

func (g *grpcClients) BrandService() content_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() content_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() content_service.ProductServiceClient {
	return g.productService
}

// remainder service methods

func (g *grpcClients) RemainderService() remainder_service.RemainderServiceClient {
	return g.remainderService
}

func (g *grpcClients) IncomeService() remainder_service.IncomeServiceClient {
	return g.incomeService
}

func (g *grpcClients) IncomeProductService() remainder_service.IncomeProductServiceClient {
	return g.incomeProductService
}

// checkout service methods

func (g *grpcClients) ShiftService() checkout_service.ShiftServiceClient {
	return g.shiftService
}

func (g *grpcClients) SaleService() checkout_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() checkout_service.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) TransactionService() checkout_service.TransactionServiceClient {
	return g.transactionService
}

func (g *grpcClients) PaymentService() checkout_service.PaymentServiceClient {
	return g.paymentService
}

// auth_service
func (g *grpcClients) AuthUserService() auth_service.UserServiceClient {
	return g.authUserService
}

func (g *grpcClients) SessionService() auth_service.SessionServiceClient {
	return g.sessionService
}
