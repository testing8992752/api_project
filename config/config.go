package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string
	Domain      string

	DefaultOffset string
	DefaultLimit  string

	OrganizationServiceHost string
	OrganizationGRPCPort    string

	ContentServiceHost string
	ContentGRPCPort    string

	RemainderServiceHost string
	RemainderGRPCPort    string

	CheckoutServiceHost string
	CheckoutGRPCPort    string

	DefaultBarCode string
	DefaultSaleId  string

	AuthServiceHost string
	AuthGRPCPort    string
	SecretKey       string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("/go-api-gateway.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	config.DefaultBarCode = cast.ToString(getOrReturnDefaultValue("BAR_CODE", ""))
	config.DefaultSaleId = cast.ToString(getOrReturnDefaultValue("SALE_ID", ""))

	config.OrganizationServiceHost = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_SERVICE_HOST", "localhost"))
	config.OrganizationGRPCPort = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_GRPC_PORT", ":9101"))

	config.ContentServiceHost = cast.ToString(getOrReturnDefaultValue("CONTENT_SERVICE_HOST", "localhost"))
	config.ContentGRPCPort = cast.ToString(getOrReturnDefaultValue("CONTENT_GRPC_PORT", ":8080"))

	config.RemainderServiceHost = cast.ToString(getOrReturnDefaultValue("REMAINDER_SERVICE_HOST", "localhost"))
	config.RemainderGRPCPort = cast.ToString(getOrReturnDefaultValue("REMAINDER_GRPC_PORT", ":8081"))

	config.CheckoutServiceHost = cast.ToString(getOrReturnDefaultValue("CHECKOUT_SERVICE_HOST", "localhost"))
	config.CheckoutGRPCPort = cast.ToString(getOrReturnDefaultValue("CHECKOUT_GRPC_PORT", ":8082"))

	config.AuthServiceHost = cast.ToString(getOrReturnDefaultValue("AUTH_SERVICE_HOST", "localhost"))
	config.AuthGRPCPort = cast.ToString(getOrReturnDefaultValue("AUTH_GRPC_PORT", ":9102"))
	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "market"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
